Application: Tag Maps

--

<div style="width:100%;position:fixed;bottom:0px;text-align:center;z-index:9999;" class="fragment" data-fragment-index="1">
<p style="vertical-align: bottom;background-color:white" >
**Tag Map for Fort Mason Center, San Francisco:**  
Number of photo locations: <span style="color:#FF8D3E">41,777</span> Contributing users: <span style="color:#FF8D3E">5002</span> Total available tags: <span style="color:#FF8D3E">197,858</span> Period (date taken): <span style="color:#FF8D3E">2007-2014</span>
</p> 
</div>

{% background data-background-image="/decoding-the-city/images/fortmason2.png" %}

Note:
Fort Mason Design Competition 2012 (Team West 8)

--

[<img class="plain"  src="/decoding-the-city/images/4steps.png"/>](https://github.com/Sieboldianus/TagMaps)  
<div class="fragment fade-out" data-fragment-index="0" style="width:100%;position:fixed;bottom:150px;text-align:right;z-index:9999;">
<p style="vertical-align: bottom;" >
**4 Steps:** </br>High Park </br>(Toronto) </br>Example
</p> 
</div>

<div class="fragment" data-fragment-index="0" style="width:100%;position:fixed;top:100px;text-align:left;z-index:9999;">
<p style="vertical-align: bottom;" >
<span class="highlight">raw data</span>
</p> 
</div>
<div class="fragment" style="width:100%;position:fixed;top:100px;text-align:right;z-index:9999;">
<p style="vertical-align: bottom;" >
<span class="highlight">cluster </br>photo </br>locations</span>
</p> 
</div>
<div class="fragment" style="width:100%;position:fixed;bottom:100px;text-align:left;z-index:9999;">
<p style="vertical-align: bottom;" >
<span class="highlight">tag </br>clustering </br> & alpha </br>shapes</span>
</p> 
</div>
<div class="fragment" style="width:100%;position:fixed;bottom:100px;text-align:right;z-index:9999;">
<p style="vertical-align: bottom;" >
<span class="highlight">soft </br>placement </br>of all </br>tag cluster</span>
</p> 
</div>

--

<video controls data-autoplay loop>
<source data-src="https://wwwpub.zih.tu-dresden.de/~s7398234/slide_assets/ClusterDistance_Ani.webm" type="video/webm" />
</video>

<div style="width:100%;position:fixed;top:0px;text-align:center;z-index:9999;">
<p class="highlight" style="vertical-align: top;" >
**Photo Location Clustering at Highpark, Toronto** 
</p> 
</div>

Note:
Data (photo location) groups itself along similar patterns of density, beginning with the highest density areas
bottom-up clustering: joining things that belong together
question: what is the best clustering distance?

--

[<img class="plain"  src="/decoding-the-city/images/03_scales.png"/>](https://ad.vgiscience.org/tagmaps/docs/theory/introduction/)  
<div style="width:100%;position:fixed;bottom:0px;text-align:center;z-index:9999;">
<p style="vertical-align: bottom;" >
Tag Maps at different scales (Greater Toronto Area)
</p> 
</div>

--

Application : Examples

--

**Monitoring**

--

**The High Line (NY)**  
<img class="plain"  src="/decoding-the-city/images/11_monitor_1.png"/>  

--

[<img class="plain"  src="/decoding-the-city/images/11_monitor.png"/>](http://maps.alexanderdunkel.com/?mapid=2)  

--

<img class="plain"  src="/decoding-the-city/images/jamescorner2.png"/>

--

**Urban Planning**

--

<div style="width:100%;position:fixed;bottom:0px;text-align:center;z-index:9999;">
<p style="vertical-align: bottom;background: rgba(255, 255, 255, 0.8)" >
**Tag-Emoji Map for Waynesboro, VA**
</p> 
</div>

{% background data-background-image="/decoding-the-city/images/waynesboro_tagmap.png" %}

--

<img class="plain"  src="/decoding-the-city/images/2019-06-24_waynesboro-panorama.jpg"/>  

<div style="width:100%;position:fixed;bottom:150px;right:50px;text-align:right;z-index:9999;">
<p style="vertical-align: bottom;" >
Waynesboro
</p> 
</div>

--

<img class="plain"  src="/decoding-the-city/images/crozet.jpg"/>  
Berrypicking

--

<img class="plain"  src="/decoding-the-city/images/wine.png"/>  
Vineyards & Craft Beer

--

<div style="width:100%;position:fixed;bottom:0px;text-align:center;z-index:9999;">
<p style="vertical-align: bottom;background: rgba(255, 255, 255, 0.8)" >
**Tag Map Yosemite Valley**
</p> 
</div>

{% background data-background-image="/decoding-the-city/images/yosemite.png" %}

--

<div style="width:100%;position:fixed;bottom:0px;text-align:center;z-index:9999;">
<p style="vertical-align: bottom;background: rgba(255, 255, 255, 0.8)" >
**Westsachsen (Regionalplanung)**
</p> 
</div>

{% background data-background-image="/decoding-the-city/images/westsachsen.png" %}


--

<div style="width:100%;position:fixed;bottom:0px;text-align:center;z-index:9999;">
<p style="vertical-align: bottom;background: rgba(255, 255, 255, 0.8)" >
**Emoji Tag Map, Campus TU Dresden**
</p> 
</div>

{% background data-background-image="/decoding-the-city/images/campus.png" %}

--

**Limitations**

--

Tag Maps bias:
* not universally represantative
* only suited to get a first overview
* highly frequented places and event locations dominate

--

<iframe class="stretch" data-src="https://ad.vgiscience.org/meingruen/meingruen_gg.html"></iframe>
