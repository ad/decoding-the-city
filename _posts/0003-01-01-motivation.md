## Motivation

--

The "Public Perception" - Motivation

## EU Landscape Convention:  

**… landscape “as a zone or area as <span class="highlight">perceived</span> by local people or visitors” (ELC art. 1, para. 38). **

Note:
- when I developed this technique, I was working in landscape planning
- often, planners or landscape architects are asked to evaluate how people perceive the landscape
- but it is very difficult to understand perception of many, sometimes thousands of people

--

Problem(s):  
**“Landscape” and perception (= valuation)  
of the landscape are inseparably intertwined.
**

<img class="plain"  src="/decoding-the-city/images/01_tress_humanlandscape_interaction.png"/>

--

<img class="plain"  src="/decoding-the-city/images/triangu2.png"/>

--

*Crowdsourced Photo Data*:
A source for planners to understand <span style="color:#FF8D3E">Collective Human Perception</span>

--

<div style="position:absolute; margin-left: auto; margin-right: auto; margin-bottom: 100px; left: 0; bottom: 0;" class="fragment">      
<p style="text-align:left;color:#00FEFF">
4 Facets:  
Location (Where?)  
User origin (Who?)  
Tags (What?)  
Time (When?)
</p> 
</div>
<video controls data-autoplay loop>
<source data-src="https://wwwpub.zih.tu-dresden.de/~s7398234/slide_assets/2017_FlickrEuropeAni.webm" type="video/webm" />
</video>


<!-- .slide: data-background="#232323" -->

Notes:
- Visualisierung 72 M Fotos 2007-2017 in Europa (Flickr)
- Verteilung nicht zufällig: deutlich Gemeinsamkeiten der Wahrnehmung vieler Menschen erkennbar (z.B. Küstenlinien)
- 4 analytische Facetten der Daten

--

<span class="highgrey">4 Facets:</span>  
From Basic to complex Visualizations

--

Photo Locations (WHERE):  

**User Frequentation**

--

**London View Management Framework (LVMF)**  
<img class="plain"  src="/decoding-the-city/images/05_lvmf_overview.png"/>  
Visual Assessment of London’s unique Landscape “as a support for visual impact assessment”
<span style="color:#FF8D3E">conducted by experts</span>

--

<img class="plain"  src="/decoding-the-city/images/06_lvmf_kenwood.png"/>  
<span style="color:#32BFF0">Blue:</span> All photo locations - <span style="color:#ED2024">Red:</span> Attribution to view of London

--

<img class="plain"  src="/decoding-the-city/images/06_lvmf_primrose.png"/>  
<span style="color:#32BFF0">Blue:</span> All photo locations - <span style="color:#ED2024">Red:</span> Attribution to view of London

--

<img class="plain"  src="/decoding-the-city/images/06_lvmf_parliament.png"/>  
<span style="color:#32BFF0">Blue:</span> All photo locations - <span style="color:#ED2024">Red:</span> Attribution to view of London

--

User origin (WHO):  
 
**Representativeness**

“Bias of Information”

--

[<img class="plain"  src="/decoding-the-city/images/07_repr_map_glob_americans.png"/>](https://flic.kr/p/DxoZjY)  
<span style="color:#FF8D3E">Americans</span>

--

[<img class="plain"  src="/decoding-the-city/images/07_repr_map_glob_europeans.png"/>](https://flic.kr/p/E417S5)  
<span style="color:#FF8D3E">Europeans</span>

--

[<img class="plain"  src="/decoding-the-city/images/08_jacobsweg.png"/>](http://blog.alexanderdunkel.com/2016/02/differing-photo-patterns-based-on-user-origin/)  
**a:** All Photo Locations  
<span class="fragment">    **b:** Photo Locations from Germans</span>  
<span class="fragment highlight">**➝** Camino de Santiago</span> 

--

Tags (WHAT):  
 
**Attribution of Meaning**

--

<div style="position:absolute; margin-left: auto; margin-right: auto; margin-top: -20px; left: 0; top: 0;z-index:2;" class="fragment" data-fragment-index="1">
<img class="plain " src="/decoding-the-city/images/09_yosemite_tunnelview.png"/>  
</div>  
<div class="stretch" style="position:relative;margin:0 auto;">
<img class="plain" src="/decoding-the-city/images/09_yosemite.png" style="position:absolute;left:15%;z-index:0" />
<img class="plain fragment " src="/decoding-the-city/images/09_yosemite_lab.png" style="position:absolute;left:15%;z-index:1" data-fragment-index="1"/>
</div>
**Weighted Sightlines in Yosemite Valley:**  
Meaning of a place as a perceived subject (**grey**) compared to its meaning as a vantage point (<span style="color:#ED2024">**red**</span>)

--

<iframe class="stretch" data-src="https://wwwpub.zih.tu-dresden.de/~s7398234/tagmaps/?mapid=0&layerid=2"></iframe>

<!-- .slide: data-background="#232323" -->

Note:
Tag Locations for two tags: GG/BB
(Explain difference to automatic tagging: Bernal Heights View)